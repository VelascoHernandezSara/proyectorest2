<?php

/**
 * $desc_entidad
 */
$app->post('/entidades', function() use ($app,$app_request, $app_response) {
            $desc_entidad = $app_request->post("desc_entidad");
            $xml = formatos($app_request);
            $data = array();
            $status = 201;
            if (!is_null($desc_entidad) && unique("entidad", "desc_entidad", $desc_entidad)) {
                try {
                    conect();
                    $result = insertEntidad($desc_entidad);
                    if($result){
                        $app->redirect('/api/v3/entidades/'.$result->clave_entidad,302);
                        /*$data= array(
                            "url"=>"/entidad/".$result->clave_entidad,
                        );*/
                    }
                } catch (Exception $e) {
                    $status = 500;
                }
            } else {
                $status = 400;
            }
            respuesta($app_response, $status, "Entidad", $data, $xml);
        });



$app->post('/entidades/:id/municipios', function($id) use ($app_request, $app_response) {
            $desc_municipio = $app_request->post("desc_municipio");
            $xml = formatos($app_request);
            $data = array();
            $status = 201;
            if (!is_null($desc_municipio) && unique("municipio", "desc_municipio", $desc_municipio)) {
                try {
                    conect();
                    $result = insertMunicipio($id, $desc_municipio);
                    if($result){
                        $app->redirect("/api/v3/entidades/$id/municipios/".$result->clave_municipio,302);
                    }
                } catch (Exception $e) {
                    $status = 500;
                }
            } else {
                $status = 400;
            }
            respuesta($app_response, $status, "Municipio", $data, $xml);
        });



$app->post('/entidades/:id/municipios/:mun/indicadores', function($id, $mun) use ($app_request, $app_response) {
            $indicador = $app_request->post("indicador");
            $id_tema_3 = $app_request->post("id_tema3");
            $xml = formatos($app_request);
            $data = array();
            $status = 201;
            if (!is_null($indicador) && !is_null($id_tema_3) && unique("indicador", "indicador", $indicador)) {
                try {
                    conect();
                    $result = insertIndicador($id, $mun, $id_tema_3, $indicador);
                    if($result){
                        $data= array(
                            "url"=>"/entidades/$id/municipios/$mun/indicadores/".$result->id_indicador,
                        );
                    }
                } catch (Exception $e) {
                    $status = 500;
                }
            } else {
                $status = 400;
            }
            respuesta($app_response, $status, "Indicador", $data, $xml);
        });



$app->post('/entidades/:id/municipios/:mun/indicadores/:ind/:anio', function($id, $mun, $ind, $anio) use ($app_request, $app_response) {
            $valor = $app_request->post("valor");
            $unidad = $app_request->post("unidad");
            $xml = formatos($app_request);
            $data = array();
            $status = 201;
            if (!is_null($valor) && is_numeric($valor) && is_string($unidad) && unique("valor", "anio", $anio)) {
                try {
                    conect();
                    $result = insertValor($id, $mun, $ind, $anio, $valor, $unidad);
                    if($result){
                        $data= array(
                            "url"=>"/entidades/$id/municipios/$mun/indicadores/$ind/$anio",
                        );
                    }
                } catch (Exception $e) {
                    $status = 500;
                }
            } else {
                $status = 400;
            }
            respuesta($app_response, $status, "Indicador", $data, $xml);
        });






$app->post('/temas1', function() use($app_request, $app_response) {
            $xml = formatos($app_request);
            $nombre_tema1 = $app_request->post("nombre_tema1");
            $datos = array();
            $status = 201;
            if (!is_null($nombre_tema1) && unique("tema1", "nombre_tema1", $nombre_tema1)) {
                try {
                    conect();
                    $result = insertTemaNivel1($nombre_tema1);
                    if($result){
                        $data= array(
                            "url"=>"/temas1/".$result->id_tema1,
                        );
                    }
                } catch (Exception $e) {
                    $status = 500;
                }
            } else {
                $status = 400;
            }
            respuesta($app_response, $status, "Tema_Nivel_1", $datos, $xml);
        });


$app->post('/temas1/:id_tema1/temas2', function($id_tema1) use($app_request, $app_response) {
            $xml = formatos($app_request);
            $nombre_tema2 = $app_request->post("nombre_tema2");
            $datos = array();
            $status = 201;
            if (!is_null($nombre_tema2)&& unique("tema2", "nombre_tema2", $nombre_tema2)) {
                try {
                    conect();
                    $result = insertTemaNivel2($id_tema1, $nombre_tema2);
                    if($result){
                        $data= array(
                            "url"=>"/temas1/$id_tema1/temas2/".$result->id_tema2,
                        );
                    }
                } catch (Exception $e) {
                    $status = 500;
                }
            } else {
                $status = 400;
            }
            respuesta($app_response, $status, "Tema_Nivel_2", $datos, $xml);
        });


$app->post('/temas1/:id_tema1/temas2/:id_tema2/temas3', function($id_tema1, $id_tema2) use($app_request, $app_response) {
            $xml = formatos($app_request);
            $nombre_tema3 = $app_request->post("nombre_tema3");
            $datos = array();
            $status = 201;
            if (!is_null($nombre_tema3) && unique("tema3", "nombre_tema3", $nombre_tema3)) {
                try {
                    conect();
                    $result = insertTemaNivel3($id_tema1, $id_tema2, $nombre_tema3);
                    if($result){
                        $data= array(
                            "url"=>"/temas1/$id_tema1/temas2/$id_tema2/temas3/".$result->id_tema3,
                        );
                    }
                } catch (Exception $e) {
                    $status = 500;
                }
            } else {
                $status = 400;
            }
            respuesta($app_response, $status, "Tema_Nivel_3", $datos, $xml);
        });


$app->post('/temas1/:id_tema1/temas2/:id_tema2/temas3/:id_tema3/indicadores', function($id_tema1, $id_tema2, $id_tema3) use($app_request, $app_response) {
            $xml = formatos($app_request);
            $indicador = $app_request->post("indicador");
            $clave_municipio = $app_request->post("clave_municipio");
            $clave_entidad = $app_request->post("clave_entidad");
            $datos = array();
            $status = 201;
            if (!is_null($indicador)&& is_numeric($clave_entidad) && is_numeric($clave_municipio) && unique("indicador", "indicador", $indicador)) {
                try {
                    conect();
                    $result = insertIndicador($clave_entidad, $clave_municipio, $id_tema3, $indicador);
                    if($result){
                        $data= array(
                            "url"=>"/temas1/$id_tema1/temas2/$id_tema2/temas3/$id_tema3/indicadores/".$result->id_indicador,
                        );
                    }
                } catch (Exception $e) {
                    $status = 500;
                }
            } else {
                $status = 400;
            }
            respuesta($app_response, $status, "Indicador", $datos, $xml);
        });

$app->post('/temas1/:id_tema1/temas2/:id_tema2/temas3/:id_tema3/indicadores/:ind/:anio', function($id_tema1, $id_tema2, $id_tema3, $ind, $anio) use($app_request, $app_response) {
            $xml = formatos($app_request);
            $valor = $app_request->post("valor");
            $unidad = $app_request->post("unidad");
            $datos = array();
            $status = 201;
            if (!is_null($valor)&& unique("valor", "anio", $anio)) {
                try {
                    conect();
                    $i=  getInd($id_tema3, $ind, $anio);
                    $result = insertValor($i->clave_entidad, $i->clave_municipio, $ind, $anio, $valor, $unidad);
                    if($result){
                        $data= array(
                            "url"=>"/temas1/$id_tema1/temas2/$id_tema2/temas3/$id_tema3/indicadores/$ind/$anio",
                        );
                    }
                } catch (Exception $e) {
                    $status = 500;
                }
            } else {
                $status = 400;
            }
            respuesta($app_response, $status, "Valor", $datos, $xml);
        });
?>
