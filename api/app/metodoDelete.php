<?php

/**
 * $desc_entidad
 */
$app->delete('/entidades/:id', function($id) use ($app_request, $app_response) {
            $xml = formatos($app_request);
            $data = array();
            $status = 200;
            try {
                conect();
                $data = deleteEntidad($id);
            } catch (Exception $e) {
                $status = 404;
            }
            respuesta($app_response, $status, "Entidad", $data, $xml);
        });



$app->delete('/entidades/:id/municipios/:mun', function($id, $mun) use ($app_request, $app_response) {
            $xml = formatos($app_request);
            $data = array();
            $status = 200;
            try {
                conect();
                $data = deleteMunicipio($id, $mun);
            } catch (Exception $e) {
                $status = 404;
            }
            respuesta($app_response, $status, "Municipio", $data, $xml);
        });



$app->delete('/entidades/:id/municipios/:mun/indicadores/:ind', function($id, $mun, $ind) use ($app_request, $app_response) {
            $xml = formatos($app_request);
            $data = array();
            $status = 200;
            try {
                conect();
                $data = deleteIndicador($id, $mun, $ind);
            } catch (Exception $e) {
                $status = 404;
            }
            respuesta($app_response, $status, "Indicador", $data, $xml);
        });



$app->delete('/entidades/:id/municipios/:mun/indicadores/:ind/:anio', function($id, $mun, $ind, $anio) use ($app_request, $app_response) {
            $xml = formatos($app_request);
            $data = array();
            $status = 200;
            try {
                conect();
                $data = deleteValor($id, $mun, $ind, $anio);
            } catch (Exception $e) {
                $status = 404;
            }
            respuesta($app_response, $status, "Indicador", $data, $xml);
        });






$app->delete('/temas1/:id_tema1', function($id_tema1) use($app_request, $app_response) {
            $xml = formatos($app_request);
            $datos = array();
            $status = 200;
            try {
                conect();
                $datos = deleteTemaNivel1($id_tema1);
            } catch (Exception $e) {
                $status = 404;
            }
            respuesta($app_response, $status, "Tema_Nivel_1", $datos, $xml);
        });


$app->delete('/temas1/:id_tema1/temas2/:id_tema2', function($id_tema1, $id_tema2) use($app_request, $app_response) {
            $xml = formatos($app_request);
            $datos = array();
            $status = 200;
            try {
                conect();
                $datos = deleteTemaNivel2($id_tema1, $id_tema2);
            } catch (Exception $e) {
                $status = 404;
            }
            respuesta($app_response, $status, "Tema_Nivel_2", $datos, $xml);
        });


$app->delete('/temas1/:id_tema1/temas2/:id_tema2/temas3/:id_tema3', function($id_tema1, $id_tema2, $id_tema3) use($app_request, $app_response) {
            $xml = formatos($app_request);
            $datos = array();
            $status = 200;
            try {
                conect();
                $datos = deleteTemaNivel3($id_tema2, $id_tema3);
            } catch (Exception $e) {
                $status = 404;
            }
            respuesta($app_response, $status, "Tema_Nivel_3", $datos, $xml);
        });


$app->delete('/temas1/:id_tema1/temas2/:id_tema2/temas3/:id_tema3/indicadores/:ind', function($id_tema1, $id_tema2, $id_tema3, $ind) use($app_request, $app_response) {
            $xml = formatos($app_request);
            $datos = array();
            $status = 200;
            try {
                conect();
                $datos = deleteIndicadorTema($id_tema3, $ind);
            } catch (Exception $e) {
                $status = 404;
            }
            respuesta($app_response, $status, "Indicador", $datos, $xml);
        });

$app->delete('/temas1/:id_tema1/temas2/:id_tema2/temas3/:id_tema3/indicadores/:ind/:anio', function($id_tema1, $id_tema2, $id_tema3, $ind, $anio) use($app_request, $app_response) {
            $xml = formatos($app_request);
            $datos = array();
            $status = 200;
            try {
                conect();
                $datos = deleteValorTema($id_tema3, $ind, $anio, $valor);
            } catch (Exception $e) {
                $status = 404;
            }
            respuesta($app_response, $status, "Valor", $datos, $xml);
        });
?>